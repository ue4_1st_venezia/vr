// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VRPROJECT_VENEZIA_VRProject_VeneziaGameModeBase_generated_h
#error "VRProject_VeneziaGameModeBase.generated.h already included, missing '#pragma once' in VRProject_VeneziaGameModeBase.h"
#endif
#define VRPROJECT_VENEZIA_VRProject_VeneziaGameModeBase_generated_h

#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_SPARSE_DATA
#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_RPC_WRAPPERS
#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAVRProject_VeneziaGameModeBase(); \
	friend struct Z_Construct_UClass_AVRProject_VeneziaGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AVRProject_VeneziaGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/VRProject_Venezia"), NO_API) \
	DECLARE_SERIALIZER(AVRProject_VeneziaGameModeBase)


#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAVRProject_VeneziaGameModeBase(); \
	friend struct Z_Construct_UClass_AVRProject_VeneziaGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AVRProject_VeneziaGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/VRProject_Venezia"), NO_API) \
	DECLARE_SERIALIZER(AVRProject_VeneziaGameModeBase)


#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AVRProject_VeneziaGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVRProject_VeneziaGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVRProject_VeneziaGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVRProject_VeneziaGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVRProject_VeneziaGameModeBase(AVRProject_VeneziaGameModeBase&&); \
	NO_API AVRProject_VeneziaGameModeBase(const AVRProject_VeneziaGameModeBase&); \
public:


#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AVRProject_VeneziaGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVRProject_VeneziaGameModeBase(AVRProject_VeneziaGameModeBase&&); \
	NO_API AVRProject_VeneziaGameModeBase(const AVRProject_VeneziaGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVRProject_VeneziaGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVRProject_VeneziaGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVRProject_VeneziaGameModeBase)


#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_12_PROLOG
#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_SPARSE_DATA \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_RPC_WRAPPERS \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_INCLASS \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_SPARSE_DATA \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VRPROJECT_VENEZIA_API UClass* StaticClass<class AVRProject_VeneziaGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
