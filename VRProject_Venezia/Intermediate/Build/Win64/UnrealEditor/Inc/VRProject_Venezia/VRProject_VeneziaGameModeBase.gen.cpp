// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VRProject_Venezia/VRProject_VeneziaGameModeBase.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVRProject_VeneziaGameModeBase() {}
// Cross Module References
	VRPROJECT_VENEZIA_API UClass* Z_Construct_UClass_AVRProject_VeneziaGameModeBase_NoRegister();
	VRPROJECT_VENEZIA_API UClass* Z_Construct_UClass_AVRProject_VeneziaGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_VRProject_Venezia();
// End Cross Module References
	void AVRProject_VeneziaGameModeBase::StaticRegisterNativesAVRProject_VeneziaGameModeBase()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AVRProject_VeneziaGameModeBase);
	UClass* Z_Construct_UClass_AVRProject_VeneziaGameModeBase_NoRegister()
	{
		return AVRProject_VeneziaGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AVRProject_VeneziaGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AVRProject_VeneziaGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_VRProject_Venezia,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVRProject_VeneziaGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "VRProject_VeneziaGameModeBase.h" },
		{ "ModuleRelativePath", "VRProject_VeneziaGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AVRProject_VeneziaGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AVRProject_VeneziaGameModeBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AVRProject_VeneziaGameModeBase_Statics::ClassParams = {
		&AVRProject_VeneziaGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AVRProject_VeneziaGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AVRProject_VeneziaGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AVRProject_VeneziaGameModeBase()
	{
		if (!Z_Registration_Info_UClass_AVRProject_VeneziaGameModeBase.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AVRProject_VeneziaGameModeBase.OuterSingleton, Z_Construct_UClass_AVRProject_VeneziaGameModeBase_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AVRProject_VeneziaGameModeBase.OuterSingleton;
	}
	template<> VRPROJECT_VENEZIA_API UClass* StaticClass<AVRProject_VeneziaGameModeBase>()
	{
		return AVRProject_VeneziaGameModeBase::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AVRProject_VeneziaGameModeBase);
	struct Z_CompiledInDeferFile_FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AVRProject_VeneziaGameModeBase, AVRProject_VeneziaGameModeBase::StaticClass, TEXT("AVRProject_VeneziaGameModeBase"), &Z_Registration_Info_UClass_AVRProject_VeneziaGameModeBase, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AVRProject_VeneziaGameModeBase), 4181698201U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_3160850042(TEXT("/Script/VRProject_Venezia"),
		Z_CompiledInDeferFile_FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_VRProject_Venezia_Source_VRProject_Venezia_VRProject_VeneziaGameModeBase_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
