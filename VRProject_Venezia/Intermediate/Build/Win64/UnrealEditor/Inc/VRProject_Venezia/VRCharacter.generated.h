// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VRPROJECT_VENEZIA_VRCharacter_generated_h
#error "VRCharacter.generated.h already included, missing '#pragma once' in VRCharacter.h"
#endif
#define VRPROJECT_VENEZIA_VRCharacter_generated_h

#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_SPARSE_DATA
#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_RPC_WRAPPERS
#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAVRCharacter(); \
	friend struct Z_Construct_UClass_AVRCharacter_Statics; \
public: \
	DECLARE_CLASS(AVRCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VRProject_Venezia"), NO_API) \
	DECLARE_SERIALIZER(AVRCharacter)


#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAVRCharacter(); \
	friend struct Z_Construct_UClass_AVRCharacter_Statics; \
public: \
	DECLARE_CLASS(AVRCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VRProject_Venezia"), NO_API) \
	DECLARE_SERIALIZER(AVRCharacter)


#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AVRCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVRCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVRCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVRCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVRCharacter(AVRCharacter&&); \
	NO_API AVRCharacter(const AVRCharacter&); \
public:


#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVRCharacter(AVRCharacter&&); \
	NO_API AVRCharacter(const AVRCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVRCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVRCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AVRCharacter)


#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_10_PROLOG
#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_SPARSE_DATA \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_RPC_WRAPPERS \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_INCLASS \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_SPARSE_DATA \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_INCLASS_NO_PURE_DECLS \
	FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VRPROJECT_VENEZIA_API UClass* StaticClass<class AVRCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_VRProject_Venezia_Source_VRProject_Venezia_VRCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
