// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVRProject_Venezia_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_VRProject_Venezia;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_VRProject_Venezia()
	{
		if (!Z_Registration_Info_UPackage__Script_VRProject_Venezia.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/VRProject_Venezia",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0x754BFA1D,
				0xAB9AC55C,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_VRProject_Venezia.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_VRProject_Venezia.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_VRProject_Venezia(Z_Construct_UPackage__Script_VRProject_Venezia, TEXT("/Script/VRProject_Venezia"), Z_Registration_Info_UPackage__Script_VRProject_Venezia, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x754BFA1D, 0xAB9AC55C));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
