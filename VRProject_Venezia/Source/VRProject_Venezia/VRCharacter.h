// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "HandController.h"
#include "VRCharacter.generated.h"

UCLASS()
class VRPROJECT_VENEZIA_API AVRCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AVRCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	//UPROPERTY float
	UPROPERTY(EditAnywhere)
		float MaxTeleportDistance = 1000;

	UPROPERTY(EditAnywhere)
		float TeleportFadeTime = 1;

	UPROPERTY(EditAnywhere)
		float TeleportSimulationRadius;

	UPROPERTY(EditAnywhere)
		float TeleportSimulationSpeed;

	UPROPERTY(EditAnywhere)
		float TeleportSimulationTime;

	//UPROPERTY FVector
	UPROPERTY(EditAnywhere)
		FVector TeleportProjectionExtent = FVector(100, 100, 100);

	//UPROPERTY class
	UPROPERTY(VisibleAnywhere)
		class UCameraComponent* Camera;

	UPROPERTY()
		class USceneComponent* VRRoot;

	UPROPERTY(EditAnywhere)
		class UStaticMeshComponent* DestinationMarker;

	UPROPERTY()
		class UPostProcessComponent* PostProcessComponent;

	UPROPERTY(EditAnywhere)
		class UMaterialInterface* BlinkerMaterialBase;

	UPROPERTY()
		class UMaterialInstanceDynamic* BlinkerMaterialInstance;

	UPROPERTY(EditAnywhere)
		class UCurveFloat* RadiusVsVelocity;

	UPROPERTY(EditAnywhere)
		class AHandController* LeftController;

	UPROPERTY(EditAnywhere)
		class AHandController* RightController;

	UPROPERTY(VisibleAnywhere)
		class USplineComponent* TeleportPath;

	UPROPERTY(EditAnywhere)
		TArray<class USplineMeshComponent*> TeleportMeshPool;

	UPROPERTY(EditAnywhere)
		class UStaticMesh* TeleportArchMesh;

	UPROPERTY(EditAnywhere)
		class UMaterialInterface* TeleportArchMaterial;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class AHandController> HandControllerClass;

	//Function
	void MoveForward(float Throttle);
	void MoveRight(float Throttle);
	bool FindTeleportDestination(TArray<FVector>& OutPath, FVector& OutLocation);
	void UpdateDestinationMarker();
	void UpdateBlinkers();
	void UpdateSpline(const TArray<FVector>& Path);
	void DrawTeleportPath(const TArray<FVector>& Path);
	void BeginTeleport();
	void FinishTeleport();
	void StartFade(float FromAlpha, float ToAlpha);	

	FVector2D GetBlinkerCentre();

	void GripLeft() { LeftController->Grip(); }
	void ReleaseLeft() { LeftController->Release(); }
	void GripRight() { RightController->Grip(); }
	void ReleaseRight() { RightController->Release(); }
};