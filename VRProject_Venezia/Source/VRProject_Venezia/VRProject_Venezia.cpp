// Copyright Epic Games, Inc. All Rights Reserved.

#include "VRProject_Venezia.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, VRProject_Venezia, "VRProject_Venezia" );
