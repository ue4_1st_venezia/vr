// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VRProject_VeneziaGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class VRPROJECT_VENEZIA_API AVRProject_VeneziaGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
